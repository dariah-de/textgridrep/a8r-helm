# Chart moved to https://gitlab.gwdg.de/dariah-de/textgridrep/charts

---

# a8r-helm

helm chart for the [aggregator](https://gitlab.gwdg.de/dariah-de/dariah-de-aggregator-services)
